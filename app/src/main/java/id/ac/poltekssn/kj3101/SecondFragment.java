package id.ac.poltekssn.kj3101;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import org.w3c.dom.Text;

import id.ac.poltekssn.kj3101.databinding.FragmentSecondBinding;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;

    private DBManager dbManager;

    private ListView listView;

    private SimpleCursorAdapter adapter;
    Cursor cursor;

    final String[] from = new String[]{DatabaseHelper._ID,
            DatabaseHelper.MATA_KULIAH, DatabaseHelper.DESC};

    final int[] to = new int[]{R.id.textViewMatKulId, R.id.textViewMatKulTitle, R.id.textViewMatKulDesc};

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentSecondBinding.inflate(inflater, container, false);

        //Shared Preferences
        getInput(binding.textViewEmailAddress, binding.textViewPassword);

        //SQLite
        dbManager = new DBManager(container.getContext());
        dbManager.open();
        cursor = dbManager.fetch();
        listView = binding.listViewMatakuliah;
        listView.setEmptyView(binding.empty);
        adapter = new SimpleCursorAdapter(container.getContext(), R.layout.matkul_record, cursor, from, to, 0);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView idTextView = (TextView) view.findViewById(R.id.textViewMatKulId);
                TextView titleTextView = (TextView) view.findViewById(R.id.textViewMatKulTitle);
                TextView descTextView = (TextView) view.findViewById(R.id.textViewMatKulDesc);

                String id = idTextView.getText().toString();
                String title = titleTextView.getText().toString();
                String desc = descTextView.getText().toString();

                // 1. Instantiate an AlertDialog.Builder with its constructor.
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // 2. Chain together various setter methods to set the dialog characteristics.
                builder.setMessage(desc)
                        .setTitle(id + ":" + title);
                // 3. Get the AlertDialog.
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        binding.buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void getInput(TextView editTextEmail, TextView editTextPass) {
        // SharedPreferences: "MySharedPref"
        // SharedPreferencesKey: (String)"email" dan (Int)"password"
        // Retrieving the value using its keys the file name must be same in both saving and retrieving the data
        SharedPreferences sh = getContext().getSharedPreferences("MySharedPref", Context.MODE_PRIVATE);
        // The value will be default as empty string because for the very
        // first time when the app is opened, there is nothing to show
        String s1 = sh.getString("email", "");
        int a = sh.getInt("password", 0);
        // We can then use the data
        editTextEmail.setText(s1);
        editTextPass.setText(String.valueOf(a));
    }

}