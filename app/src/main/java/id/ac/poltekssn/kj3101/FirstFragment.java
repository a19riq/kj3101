package id.ac.poltekssn.kj3101;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import id.ac.poltekssn.kj3101.databinding.FragmentFirstBinding;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;

    private DBManager dbManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentFirstBinding.inflate(inflater, container, false);
        dbManager = new DBManager(container.getContext());
        dbManager.open();
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonSaveToSp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.editTextTextEmailAddress.getText().length() > 0 && binding.editTextNumberPincode.getText().length() > 0) {
                    String email = binding.editTextTextEmailAddress.getText().toString();
                    int password = Integer.parseInt(binding.editTextNumberPincode.getText().toString());
                    saveInputToSharedPreferences(email, password);
                    NavHostFragment.findNavController(FirstFragment.this)
                            .navigate(R.id.action_FirstFragment_to_SecondFragment);
                } else {
                    binding.editTextTextEmailAddress.setError("Check!");
                    binding.editTextNumberPincode.setError("Check!");
                }
            }
        });

        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.editTextMataKuliah.getText().length() > 0 && binding.editTextDescription.getText().length() > 0) {
                    saveInputToSQLite(binding.editTextMataKuliah, binding.editTextDescription);
                    NavHostFragment.findNavController(FirstFragment.this)
                            .navigate(R.id.action_FirstFragment_to_SecondFragment);
                } else {
                    binding.editTextMataKuliah.setError("Check!");
                    binding.editTextDescription.setError("Check!");
                }

            }
        });
    }

    public void saveInputToSharedPreferences(String emailInput, int passwordInput) {
        // Storing data into SharedPreferences
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("MySharedPref", Context.MODE_PRIVATE);

        // Creating an Editor object to edit(write to the file)
        SharedPreferences.Editor mySharedPref = sharedPreferences.edit();

        // Storing the key and its value as the data fetched from edittext
        mySharedPref.putString("email", emailInput);
        mySharedPref.putInt("password", passwordInput);

        // Once the changes have been made, we need to commit to apply those changes made,
        // otherwise, it will throw an error
        mySharedPref.commit();
    }

    public void saveInputToSQLite(EditText title, EditText desc) {
        dbManager.insert(title.getText().toString(), desc.getText().toString());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}